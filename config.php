<?php
// HTTP
define('HTTP_SERVER', 'http://local.oc-training.com/');

// HTTPS
define('HTTPS_SERVER', 'http://local.oc-training.com/');

// DIR
define('DIR_APPLICATION', '/vagrant/catalog/');
define('DIR_SYSTEM', '/vagrant/system/');
define('DIR_IMAGE', '/vagrant/image/');
define('DIR_LANGUAGE', '/vagrant/catalog/language/');
define('DIR_TEMPLATE', '/vagrant/catalog/view/theme/');
define('DIR_CONFIG', '/vagrant/system/config/');
define('DIR_CACHE', '/vagrant/system/storage/cache/');
define('DIR_DOWNLOAD', '/vagrant/system/storage/download/');
define('DIR_LOGS', '/vagrant/system/storage/logs/');
define('DIR_MODIFICATION', '/vagrant/system/storage/modification/');
define('DIR_UPLOAD', '/vagrant/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '1234dev$#@!');
define('DB_DATABASE', 'training');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
