<?php
// HTTP
define('HTTP_SERVER', 'http://local.oc-training.com/admin/');
define('HTTP_CATALOG', 'http://local.oc-training.com/');

// HTTPS
define('HTTPS_SERVER', 'http://local.oc-training.com/admin/');
define('HTTPS_CATALOG', 'http://local.oc-training.com/');

// DIR
define('DIR_APPLICATION', '/vagrant/admin/');
define('DIR_SYSTEM', '/vagrant/system/');
define('DIR_IMAGE', '/vagrant/image/');
define('DIR_LANGUAGE', '/vagrant/admin/language/');
define('DIR_TEMPLATE', '/vagrant/admin/view/template/');
define('DIR_CONFIG', '/vagrant/system/config/');
define('DIR_CACHE', '/vagrant/system/storage/cache/');
define('DIR_DOWNLOAD', '/vagrant/system/storage/download/');
define('DIR_LOGS', '/vagrant/system/storage/logs/');
define('DIR_MODIFICATION', '/vagrant/system/storage/modification/');
define('DIR_UPLOAD', '/vagrant/system/storage/upload/');
define('DIR_CATALOG', '/vagrant/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '1234dev$#@!');
define('DB_DATABASE', 'training');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
